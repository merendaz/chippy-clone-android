package com.giselletavares.chippy_clone;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;

public class GameEngine extends SurfaceView implements Runnable
{
    // Android debug variables
    static String TAG = "CHIPPY";

    // screen size
    int screenHeight;
    int screenWidth;
    int bossPixelXPosition;
    int bossPixelYPosition;
    int bossPixelWidth;
    int bossPixelHeight;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    int quantLoops = 0;
    int quantBulletsForWalls = 55;
    int initialHpBoss = 150;
    int initialHpPlayer = 20;
    int playerBulletSpeed = 30;
    int bossBulletSpeed = 20;
    int worldBulletSpeed = 5;
    int bossInterval = 20;
    int worldInterval = 30;
    int playerInterval = 2;
//    String winLoseLabel = "";
    boolean isBossMovingRight = false;
    boolean isWorldMovingRight = true;
    boolean isWorldMovingDown = true;
    boolean isPowerUpsAppeared = false;
    boolean hasPowerUpCannon = false;
    boolean hasPowerUpSlowmo = false;

    // ------------------------------------------------
    // GAME SPECIFIC VARIABLES
    // ------------------------------------------------
    Point humanPlayerTapPosition;
    Point playerBulletTarget;
    int playerScore = 0;

    // ------------------------------------------------
    // ## SPRITES
    // ------------------------------------------------
    Sprite player;
    Sprite boss;
    Sprite world;
    Sprite powerupCannon;
    Sprite powerupSlowmo;
//    Sprite bossLayer;
    List<Sprite> bossLayerArray;
    List<Bullet> playerBulletArray;
    List<Bullet> bossBulletArray;
    List<Bullet> worldBulletArray;

    // ------------------------------------------------
    // ## SOUND EFFECTS
    // ------------------------------------------------
    SoundPool soundPool;
    SoundPool.Builder soundPoolBuilder;

    AudioAttributes attributes;
    AudioAttributes.Builder attributesBuilder;

    int soundID_lasershot1;
    int soundID_pulsedrone1cut;
    int soundID_bulletonbosslayer;
    int soundID_bulletonbosslayer1;
    int soundID_atomicbomb;
    int soundID_barrelexploding;

    // ------------------------------------------------
    // ## GAME STATS
    // ------------------------------------------------

    public GameEngine(Context context, int w, int h)
    {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();

        this.initialValues();

    }

    public void initialValues(){
        this.bossPixelXPosition = this.screenWidth / 2;
        this.bossPixelYPosition = 1;

        this.bossPixelWidth = 21;
        this.bossPixelHeight = 21;


        // Sound Effects:
        attributesBuilder = new AudioAttributes.Builder();
        attributesBuilder.setUsage(AudioAttributes.USAGE_GAME);
        attributesBuilder.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
        attributes = attributesBuilder.build();

        soundPoolBuilder = new SoundPool.Builder();
        soundPoolBuilder.setAudioAttributes(attributes);
        soundPoolBuilder.setMaxStreams(30);
        soundPool = soundPoolBuilder.build();

        soundID_lasershot1 = soundPool.load(getContext(), R.raw.lasershot1, 1);
        soundID_pulsedrone1cut = soundPool.load(getContext(), R.raw.pulsedrone1cut, 1);
        soundID_bulletonbosslayer = soundPool.load(getContext(), R.raw.bulletonbosslayer, 1);
        soundID_bulletonbosslayer1 = soundPool.load(getContext(), R.raw.bulletonbosslayer1, 1);
        soundID_atomicbomb = soundPool.load(getContext(), R.raw.atomicbomb, 1);
        soundID_barrelexploding = soundPool.load(getContext(), R.raw.barrelexploding, 1);


        // @TODO: Add your sprites
        player = new Sprite(getContext(), screenWidth / 10, screenHeight / 10, R.drawable.spaceship2);
        player.setHp(initialHpPlayer);
        boss = new Sprite(getContext(), screenWidth / 2, screenHeight / 2, R.drawable.bosseyesquare2);
        boss.setSpeed(10);
        boss.setHp(initialHpBoss);
        world = new Sprite(getContext(), -1000, -1000, R.drawable.background1);
        powerupCannon = new Sprite(getContext(), -1000, -1000, R.drawable.power_up_cannon);
        powerupSlowmo = new Sprite(getContext(), -1000, -1000, R.drawable.power_up_slowmo);

        bossLayerArray = new ArrayList<>();
        this.buildPixelBossLayer(getContext());

        this.playerBulletTarget = new Point(this.screenWidth / 2, this.screenHeight / 2);
        this.playerBulletArray = new ArrayList<>();

        this.bossBulletArray = new ArrayList<>();

        this.worldBulletArray = new ArrayList<>();
    }

    public void buildRectLayer(Context context, int x, int y, int xw, int xh, int lwh, int quantRectSide, List<Sprite> arrayOfSprites, LayerPixelColor color)
    {
//        List<Sprite> arrayOfSprites = this.bossLayerArray;
//        int quantRectSide = 14;
//        int x = this.boss.getX();
//        int y = this.boss.getY();
//        int xw = this.boss.getWidth();
//        int xh = this.boss.getHeight();
//        int lwh = this.bossPixelHeight;

        Sprite previousLeft = new Sprite(context, x - lwh, y, lwh, lwh, color);
        Sprite previousTop = new Sprite(context, x, y - lwh, lwh, lwh, color);
        Sprite previousRight = new Sprite(context, x + xw, y, lwh, lwh, color);
        Sprite previousBottom = new Sprite(context, x, y + xh, lwh, lwh, color);
        arrayOfSprites.add(previousLeft);
        arrayOfSprites.add(previousTop);
        arrayOfSprites.add(previousRight);
        arrayOfSprites.add(previousBottom);

        for(int i = 0 ; i < quantRectSide - 1 ; i++)
        {
            previousLeft = new Sprite(context, previousLeft.getX(), previousLeft.getY() + lwh, lwh, lwh, color);
            previousTop = new Sprite(context, previousTop.getX() + lwh, previousTop.getY(), lwh, lwh, color);
            previousRight = new Sprite(context, previousRight.getX(), previousRight.getY() + lwh, lwh, lwh, color);
            previousBottom = new Sprite(context, previousBottom.getX() + lwh, previousBottom.getY(), lwh, lwh, color);

            arrayOfSprites.add(previousLeft);
            arrayOfSprites.add(previousTop);
            arrayOfSprites.add(previousRight);
            arrayOfSprites.add(previousBottom);
        }

        Sprite leftTop = new Sprite(context, x - lwh, y - lwh, lwh, lwh, color);
        Sprite rightTop = new Sprite(context, x + xw, y - lwh, lwh, lwh, color);
        Sprite leftBottom = new Sprite(context, x - lwh, y + xh, lwh, lwh, color);
        Sprite rightBottom = new Sprite(context, x + xw, y + xh, lwh, lwh, color);
        arrayOfSprites.add(leftTop);
        arrayOfSprites.add(rightTop);
        arrayOfSprites.add(leftBottom);
        arrayOfSprites.add(rightBottom);
    }

    public void buildPixelBossLayer(Context context)
    {
        this.buildRectLayer(context, this.boss.getX(), this.boss.getY(), this.boss.getWidth(), this.boss.getHeight(), this.bossPixelWidth, 14, this.bossLayerArray, LayerPixelColor.RED);
        this.buildRectLayer(context, this.boss.getX() - this.bossPixelWidth, this.boss.getY() - this.bossPixelHeight, this.boss.getWidth() + (this.bossPixelWidth * 2), this.boss.getHeight() + (this.bossPixelHeight * 2), this.bossPixelWidth, 16, this.bossLayerArray, LayerPixelColor.BLUE);
        this.buildRectLayer(context, this.boss.getX() - (this.bossPixelWidth * 2), this.boss.getY() - (this.bossPixelHeight * 2), this.boss.getWidth() + (this.bossPixelWidth * 4), this.boss.getHeight() + (this.bossPixelHeight * 4), this.bossPixelWidth, 18, this.bossLayerArray, LayerPixelColor.GREEN);
    }

    public void fireAllDirectionsPatternBossBullets(int x, int y)
    {
        int xTarget = -50;
        int yTarget = -50;

        while (xTarget <= screenWidth)
        {
            xTarget = xTarget + (screenWidth * this.quantBulletsForWalls / 100);
            Rect bullet = new Rect(x, y, x + 20, y + 20);
            Point target = new Point(xTarget, yTarget);
            this.bossBulletArray.add(new Bullet(bullet, target, this.bossBulletSpeed));
        }

        while (yTarget <= screenHeight)
        {
            yTarget = yTarget + (screenHeight * this.quantBulletsForWalls / 100);
            Rect bullet = new Rect(x, y, x + 20, y + 20);
            Point target = new Point(xTarget, yTarget);
            this.bossBulletArray.add(new Bullet(bullet, target, this.bossBulletSpeed));
        }

        while (xTarget >= -50)
        {
            xTarget = xTarget - (screenWidth * this.quantBulletsForWalls / 100);
            Rect bullet = new Rect(x, y, x + 20, y + 20);
            Point target = new Point(xTarget, yTarget);
            this.bossBulletArray.add(new Bullet(bullet, target, this.bossBulletSpeed));
        }

        while (yTarget >= -50)
        {
            yTarget = yTarget - (screenHeight * this.quantBulletsForWalls / 100);
            Rect bullet = new Rect(x, y, x + 20, y + 20);
            Point target = new Point(xTarget, yTarget);
            this.bossBulletArray.add(new Bullet(bullet, target, this.bossBulletSpeed));
        }
    }

    public void fireAllDirectionsPatternWorldBullets(int x, int y)
    {
        int xTarget = 50;
        int yTarget = 50;

        while (xTarget <= screenWidth)
        {
            xTarget = xTarget + (screenWidth * this.quantBulletsForWalls / 5);
            Rect bullet = new Rect(x, y, x + 100, y + 100);
            Point target = new Point(xTarget, yTarget);
            this.worldBulletArray.add(new Bullet(bullet, target, this.worldBulletSpeed));
        }

        while (yTarget <= screenHeight)
        {
            yTarget = yTarget + (screenHeight * this.quantBulletsForWalls / 5);
            Rect bullet = new Rect(x, y, x + 100, y + 100);
            Point target = new Point(xTarget, yTarget);
            this.worldBulletArray.add(new Bullet(bullet, target, this.worldBulletSpeed));
        }

        while (xTarget >= 50)
        {
            xTarget = xTarget - (screenWidth * this.quantBulletsForWalls / 5);
            Rect bullet = new Rect(x, y, x + 100, y + 100);
            Point target = new Point(xTarget, yTarget);
            this.worldBulletArray.add(new Bullet(bullet, target, this.worldBulletSpeed));
        }

        while (yTarget >= 50)
        {
            yTarget = yTarget - (screenHeight * this.quantBulletsForWalls / 5);
            Rect bullet = new Rect(x, y, x + 100, y + 100);
            Point target = new Point(xTarget, yTarget);
            this.worldBulletArray.add(new Bullet(bullet, target, this.worldBulletSpeed));
        }
    }

    private void printScreenInfo() {
        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }



    // ------------------------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------------------------
    @Override
    public void run()
    {
        while (true)
        {
            if(gameIsRunning)
            {
                this.updatePositions();
                this.redrawSprites();
                this.setFPS();
            }
        }
    }

    public void startGame()
    {
        gameIsRunning = true;

        this.playerScore = 0;
        this.initialHpBoss = 150;
        this.initialHpPlayer = 20;

        gameThread = new Thread(this);
        gameThread.start();
    }

    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void resumeGame() { gameIsRunning = true; }

    public void gameOver(){
        if (this.holder.getSurface().isValid()) {

            this.canvas = this.holder.lockCanvas();
            this.canvas.drawBitmap(this.world.getSpriteImage(), this.world.getX(), this.world.getY(), paintbrush);
            paintbrush.setColor(Color.WHITE);
            paintbrush.setTextSize(200);

            if(this.boss.getHp() <= 0){
                canvas.drawBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.youwin), 100, screenHeight / 4, paintbrush);
            } else {
                canvas.drawBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.youlose), 100, screenHeight / 4, paintbrush);
            }

            canvas.drawText("Score: " + this.playerScore, 100, screenHeight - 700, paintbrush);
            paintbrush.setColor(Color.parseColor(LayerPixelColor.CYAN.getValue()));
            paintbrush.setTextSize(100);
            canvas.drawText("TAP to PLAY AGAIN", 100, screenHeight - 500, paintbrush);
            this.holder.unlockCanvasAndPost(canvas);

            this.initialValues();
            this.pauseGame();
        }
    }


    // ------------------------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------------------------

    public void updatePositions()
    {
        this.quantLoops++;
        List<Bullet> bulletsToBeRemoved = new ArrayList<>();
        List<Sprite> bossLayerToBeRemoved = new ArrayList<>();
        List<Bullet> bulletsBossToBeRemoved = new ArrayList<>();
        List<Bullet> bulletsWorldToBeRemoved = new ArrayList<>();

        // @TODO: Update position of all sprites
        if(humanPlayerTapPosition != null) player.moveTowards(humanPlayerTapPosition);

        if(quantLoops % this.playerInterval == 0)
        {
            int newBulletX = this.player.getX() + ((this.player.getHitBox().right - this.player.getX()) / 2);
            int newBulletY = this.player.getY() + ((this.player.getHitBox().bottom - this.player.getY()) / 2);
            Rect bulletRect = new Rect(newBulletX, newBulletY, newBulletX + 1, newBulletY + 1);
            Bullet newBullet = new Bullet(bulletRect, this.playerBulletTarget, this.playerBulletSpeed);
            this.playerBulletArray.add(newBullet);
        }

        for (Bullet bullet : this.playerBulletArray)
        {
            bullet.updatePosition();
            if (quantLoops % 3 == 0) {
                soundPool.play(soundID_lasershot1, 1, 1, 3, 0, 1);
            }

            if(bullet.isBulletOnDestination()) bulletsToBeRemoved.add(bullet);

            if(bullet.getBullet().intersect(boss.getHitBox()))
            {
                boss.setHp(boss.getHp() - 1);
                bulletsToBeRemoved.add(bullet);
                int bossX = boss.getX();
                int bossY = boss.getY();

                // ====================
                // After explosion: => REVER!!
                // ====================
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo1);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo2);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo3);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo4);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo5);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo6);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo7);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo8);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo9);
//                boss = new Sprite(this.getContext(), bossX, bossY, R.drawable.bubble_explo10);
//                soundPool.play(soundID_atomicbomb, 1, 1, 4, 0, 1);
            }
        }

        for (Bullet bullet : this.bossBulletArray)
        {
            bullet.updatePosition();
            if(bullet.isBulletOnDestination()) bulletsBossToBeRemoved.add(bullet);

            if(bullet.getBullet().intersect(this.player.getHitBox()))
            {
                soundPool.play(soundID_barrelexploding, 1,1,2,0,1);
                this.player.setHp(this.player.getHp() - 1);
                bulletsBossToBeRemoved.add(bullet);
            }
        }

        for (Bullet bullet : this.worldBulletArray)
        {
            bullet.updatePosition();
            if(bullet.isBulletOnDestination()) bulletsWorldToBeRemoved.add(bullet);

            if(bullet.getBullet().intersect(this.player.getHitBox()))
            {
                soundPool.play(soundID_barrelexploding, 1,1,2,0,1);
                this.player.setHp(this.player.getHp() - 1);
                bulletsWorldToBeRemoved.add(bullet);
            }
        }

        // @TODO: Logic of the game
        if(quantLoops % 10 == 0) this.playerScore++;

        if(quantLoops % this.bossInterval == 0)
        {
            // SPAWN BOSS BULLETS
            int xToBeSumomned = this.boss.getX() + this.boss.getWidth() / 2;
            int yToBeSummoned = this.boss.getY() + this.boss.getHeight() / 2;

            this.fireAllDirectionsPatternBossBullets(xToBeSumomned, yToBeSummoned);
            soundPool.play(soundID_pulsedrone1cut, 1,1,2,0,1);
        }

        if(quantLoops % this.worldInterval == 0)
        {
            // SPAWN WORLD BULLETS
            int xToBeSumomned = this.world.getX() + this.world.getWidth() / 2;
            int yToBeSummoned = this.world.getY() + this.world.getHeight() / 2;

            this.fireAllDirectionsPatternWorldBullets(xToBeSumomned, yToBeSummoned);
            soundPool.play(soundID_pulsedrone1cut, 1,1,2,0,1);
        }

        boolean hitTarget = false;
        for (Sprite littleRectBoss : this.bossLayerArray)
        {
            for (Bullet bullet : this.playerBulletArray)
            {
                if(bullet.getBullet().intersect(littleRectBoss.getHitBox()))
                {
                    soundPool.play(soundID_bulletonbosslayer, 1, 1, 4, 0, 1);
                    bulletsToBeRemoved.add(bullet);
                    int thisPartOfTheBossHp = littleRectBoss.getHp();
                    thisPartOfTheBossHp--;
                    littleRectBoss.setHp(thisPartOfTheBossHp);
                    if(thisPartOfTheBossHp < 0)
                    {
                        bossLayerToBeRemoved.add(littleRectBoss);
                    }
                    hitTarget = true;
                    break;
                }
            }
            if(hitTarget) break;
        }

        // power ups
        if(this.boss.getHp() == initialHpBoss * 0.8){
            this.powerupSlowmo.setX(this.boss.getX());
            this.powerupSlowmo.setY(this.boss.getY());
            this.powerupCannon.setX(this.boss.getX());
            this.powerupCannon.setY(this.boss.getY());

            this.updateSetHitBox(powerupCannon);
            this.updateSetHitBox(powerupSlowmo);

            this.isPowerUpsAppeared = true;
        }

        if(this.isPowerUpsAppeared && !this.player.getHitBox().intersect(this.powerupSlowmo.getHitBox()) && !this.player.getHitBox().intersect(this.powerupCannon.getHitBox())){
            this.powerupSlowmo.setX(this.powerupSlowmo.getX() - 5);
            this.powerupSlowmo.setY(this.powerupSlowmo.getY() - 5);
            this.powerupCannon.setX(this.powerupCannon.getX() - 7);
            this.powerupCannon.setY(this.powerupCannon.getY() - 7);

            this.updateSetHitBox(powerupCannon);
            this.updateSetHitBox(powerupSlowmo);
        }

        if(this.isPowerUpsAppeared && this.player.getHitBox().intersect(this.powerupSlowmo.getHitBox()))
        {
            this.initialPowerUpsStartPosition(this.powerupCannon);

            this.hasPowerUpSlowmo = true;
            this.isPowerUpsAppeared = false;
            this.powerupSlowmo.setX(screenWidth - 200);
            this.powerupSlowmo.setY(20);
            this.updateSetHitBox(this.powerupSlowmo);
        }

        if(this.isPowerUpsAppeared && this.player.getHitBox().intersect(this.powerupCannon.getHitBox()))
        {
            this.initialPowerUpsStartPosition(this.powerupSlowmo);
            this.hasPowerUpCannon = true;
            this.isPowerUpsAppeared = false;
            this.powerupCannon.setX(screenWidth - 200);
            this.powerupCannon.setY(20);
            this.updateSetHitBox(this.powerupCannon);
        }

        if(this.hasPowerUpSlowmo && this.player.getHitBox().intersect(this.powerupSlowmo.getHitBox())){
            this.bossInterval *= 2.5;
            this.worldInterval *= 2.5;
            this.hasPowerUpSlowmo = false;
            this.initialPowerUpsStartPosition(this.powerupSlowmo);
        }

        if(this.hasPowerUpCannon && this.player.getHitBox().intersect(this.powerupCannon.getHitBox())){
            this.playerInterval /= 2;
            this.hasPowerUpCannon = false;
            this.initialPowerUpsStartPosition(this.powerupCannon);
        }

        // Move the boss and its pixels
        if(quantLoops % 3 == 0){
            this.boss.setX(this.boss.getX() + (this.isBossMovingRight ? 10 : -10));
            this.boss.setY(this.boss.getY() + (this.isBossMovingRight ? 10 : -10));

            this.updateSetHitBox(this.boss);

            for(Sprite pixelBossLayer: this.bossLayerArray){
                pixelBossLayer.setX(pixelBossLayer.getX() + (this.isBossMovingRight ? 10 : -10));
                pixelBossLayer.setY(pixelBossLayer.getY() + (this.isBossMovingRight ? 10 : -10));
            }

            this.isBossMovingRight = !this.isBossMovingRight;
        }


        // Move the background/world
        if(quantLoops % 3 == 0){
            this.world.setX(this.world.getX() + (this.isWorldMovingRight ? 10 : -10));
        }

        if(quantLoops % 2 == 0){
            this.world.setY(this.world.getY() + (this.isWorldMovingDown ? 5 : -5));
        }

        if(quantLoops % 20 == 0) {
            this.isWorldMovingRight = !this.isWorldMovingRight;
            this.isWorldMovingDown = !this.isWorldMovingDown;
        }


        //@TODO: Test lose conditions
        if(this.player.getHp() <= 0)
        {
            // YOU LOSE
//            this.winLoseLabel = "You Lose!";
            this.gameOver();
        }

        //@TODO: Test win conditions
        if(this.boss.getHp() <= 0)
        {
            // YOU WIN!
//            this.winLoseLabel = "You Win!";
            this.gameOver();
        }

        this.playerBulletArray.removeAll(bulletsToBeRemoved);
        this.bossLayerArray.removeAll(bossLayerToBeRemoved);
        this.bossBulletArray.removeAll(bulletsBossToBeRemoved);
        this.worldBulletArray.removeAll(bulletsWorldToBeRemoved);

    }

    public void initialPowerUpsStartPosition(Sprite sprite){
        sprite.setX(-1000);
        sprite.setY(-1000);

        this.updateSetHitBox(sprite);
    }

    public void updateSetHitBox(Sprite sprite){
        sprite.setHitBox(new Rect(sprite.getX(),
                sprite.getY(),
                sprite.getX() + sprite.getSpriteImage().getWidth(),
                sprite.getY() + sprite.getSpriteImage().getHeight()));
    }

    public void redrawSprites()
    {
        if (this.holder.getSurface().isValid())
        {
            // ------------------------------------------------
            this.canvas = this.holder.lockCanvas();
            // ------------------------------------------------

            // configure the drawing tools
            this.canvas.drawColor(Color.BLACK);
            this.paintbrush.setColor(Color.BLACK);
            this.paintbrush.setStyle(Paint.Style.STROKE);
            this.paintbrush.setStrokeWidth(5);



            //@TODO: Draw sprites
            // Background/World
            canvas.drawBitmap(this.world.getSpriteImage(), this.world.getX(), this.world.getY(), paintbrush);

            // Player
            canvas.drawBitmap(this.player.getSpriteImage(), this.player.getX(), this.player.getY(), paintbrush);
            canvas.drawRect(this.player.getHitBox(), paintbrush);

            // Boss
            canvas.save();
            //canvas.rotate(45, this.boss.getX() + this.boss.getWidth() / 2, this.boss.getY() + this.boss.getWidth() / 2);
            canvas.drawBitmap(this.boss.getSpriteImage(), this.boss.getX(), this.boss.getY(), paintbrush);
            canvas.drawRect(this.boss.getHitBox(),paintbrush);
            canvas.restore();

            // Power up Cannon
            canvas.drawBitmap(this.powerupCannon.getSpriteImage(), this.powerupCannon.getX(), this.powerupCannon.getY(), paintbrush);
            canvas.drawRect(this.powerupCannon.getHitBox(), paintbrush);

            // Power up Slowmo
            canvas.drawBitmap(this.powerupSlowmo.getSpriteImage(), this.powerupSlowmo.getX(), this.powerupSlowmo.getY(), paintbrush);
            canvas.drawRect(this.powerupSlowmo.getHitBox(), paintbrush);

            // Player bullets
            this.paintbrush.setColor(Color.YELLOW);
            for (Bullet bullet : this.playerBulletArray) canvas.drawRect(bullet.getBullet(), paintbrush);
            this.paintbrush.setColor(Color.BLACK);

            // Boss bullets
            this.paintbrush.setColor(Color.RED);
            for (Bullet bullet : this.bossBulletArray) canvas.drawRect(bullet.getBullet(), paintbrush);
            this.paintbrush.setColor(Color.BLACK);

            // World bullets
            this.paintbrush.setColor(Color.GREEN);
            for (Bullet bullet : this.worldBulletArray) canvas.drawRect(bullet.getBullet(), paintbrush);
            this.paintbrush.setColor(Color.BLACK);

//            this.paintbrush.setStyle(Paint.Style.STROKE);
            this.paintbrush.setStyle(Paint.Style.FILL);
            for (Sprite sp : this.bossLayerArray){
                try {
                    this.paintbrush.setColor(Color.parseColor(sp.getPixelBossLayerColor().getValue()));
                } catch (Exception e){

                }

                this.buildTheBossLayer(sp);
            }
            this.paintbrush.setStyle(Paint.Style.STROKE);

            //@TODO: Draw game stats
            this.paintbrush.setColor(Color.BLUE);
            this.paintbrush.setTextSize(80);
            canvas.drawText("SCORE: " + this.playerScore + " sec", 100, 100, paintbrush);

            canvas.drawText("BOSS HP: " + this.boss.getHp(), 750, 100, paintbrush);

            canvas.drawText("PLAYER HP: " + this.player.getHp(), 1500, 100, paintbrush);

            // ------------------------------------------------
            this.holder.unlockCanvasAndPost(canvas);
            // ------------------------------------------------
        }
    }

    // temp method. it must be able to set any direction of layer. find the whole pattern
    public void buildTheBossLayer(Sprite sprite){
        canvas.save();
//        canvas.rotate(45, this.boss.getX() + this.boss.getWidth() / 2, this.boss.getY() + this.boss.getWidth() / 2);
        canvas.drawRect(sprite.getHitBox(),paintbrush);
        canvas.restore();
    }

    public void setFPS()
    {
        //@TODO: How many times the screen will update
        try { gameThread.sleep(60); }
        catch (Exception e) { }
    }

    // ------------------------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?

        int x = (int) event.getX();
        int y = (int) event.getY();

        int middleOfOPlayerX = (int) event.getX() - this.player.getSpriteImage().getWidth() / 2;
        int middleOfPlayerY = (int) event.getY() - this.player.getSpriteImage().getHeight() / 2;
        humanPlayerTapPosition = new Point(middleOfOPlayerX, middleOfPlayerY);

        if (userAction == MotionEvent.ACTION_DOWN) {
            if(!this.gameIsRunning){
                this.startGame();
            } else {
                Rect tapPosition = new Rect(x, y, x + 1, y + 1);
                boolean isTappingOnPlayer = this.player.getHitBox().intersect(tapPosition);
                this.player.setIsMoving(isTappingOnPlayer);
                if (!isTappingOnPlayer) this.playerBulletTarget = new Point(x, y);
            }

        } else if (userAction == MotionEvent.ACTION_UP) {
            this.player.setIsMoving(false);
        }

        return true;
    }
}
