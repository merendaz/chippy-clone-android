## Chippy Clone

# Group Members

- Antonio Merendaz - C0741427 
- Carlos Bulado - C0734506
- Giselle Tavares - C0744277


## Materials
Small guide: [https://steamcommunity.com/sharedfiles/filedetails/?id=1817375233]()

Gameplay: (all bosses) [https://www.youtube.com/watch?v=7wckBaZc_0o]()

## Level chosen 
NEOPHYTE

**Boss inspired: 3rd boss** with power ups, different colors of layers. The blue ones is tough to destruct then the green ones. The red ones are harder to destruct.

## To Do List
You are not required to make the app EXACTLY like Chippy. 
However, the game should RESEMBLE the mechanics and gameplay in the original Chippy.

At minimum, you must implement the following features.

* ✅ 1 player
* ✅ 1 enemy
	* ✅ Enemy has a core
	* ✅ Enemy has pieces/layers → You cannot directly kill the enemy. You kill parts of the enemy’s body before you can reach the core. In Chippy, this is shown as  “chipping away” at the enemy before you reach the core
	* ✅ Enemy shoots bullets in a pattern. Implement a:
		* Burst pattern; and,
		* Pattern that targets the pattern
* ✅ Player can move in all directions
* 🚩 Enemy moves in the same style as in the Chippy game
* ✅ Goal of game is to destroy the enemy
* ✅ Player can acquire powerups (shield, better gun) → Implement minimum 2
* ✅ Game tracks the progress player has made toward defeating the enemy 
	* Example - In chippy, they update the enemy graphic every time you shoot a piece of the enemy. This  represent your progress to the core.
* ✅ Game over if the Enemy destroys player
* ✅ Player has life (or) healthbar. Once all lives (or) health is used up, player dies
* ✅ World occasionally makes obstacles. Enemy is immune to obstacles.
* ✅ Obstacles and enemy behave with predictable randomness (example: it appears like the enemy bullets/movement is random, but actually it follows a repeatable pattern)
* ✅ Scoring is based on time elapsed
* ✅ Game can be restarted after game win / game loss
* ✅ Appropriate sound and art
* ✅ Game stats are displayed on screen (time elapsed, lives, available powerups, etc)

-------------
**✅ Done**

**🚩 Doing**

## Others
Decisions you must make:

* The original Chippy game uses a mouse and keyboard. You must make a decision for how to control the player on a mobile device (tapping, swiping, virtual buttons, etc)
* The original Chippy is designed for large screens. You must make a decision on how to scale the game to work on small screens. One easy suggestion would be to build the game for Android tablets.
* Some parts of the level may not be easily “translated” into a mobile device. For those components, you must make a decision of how to “translate it” / reinterpret the feature to work with mobile.

**Resources - Chippy Game**

Chippy on Steam: [https://store.steampowered.com/app/602700/Chippy/]()


**Sound and art resources:**

* Game Art Guppy: [https://www.gameartguppy.com/]()
* Kenney.nl: [https://kenney.nl/assets]()
* Freesound.com: [https://freesound.org/browse/]()
* Soundbible: [http://soundbible.com/free-sound-effects-1.html]()
